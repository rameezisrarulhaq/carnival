
<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<link href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/carnival.png" rel="shortcut icon" type="image/vnd.microsoft.icon" />

<?php
/** @var JDocumentHtml $this */

$app  = JFactory::getApplication();
$user = JFactory::getUser();

// Output as HTML5
$this->setHtml5(true);

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = htmlspecialchars($app->get('sitename'), ENT_QUOTES, 'UTF-8');

if ($task === 'edit' || $layout === 'form')
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add template js
JHtml::_('script', 'template.js', array('version' => 'auto', 'relative' => true));

// Add html5 shiv
JHtml::_('script', 'jui/html5.js', array('version' => 'auto', 'relative' => true, 'conditional' => 'lt IE 9'));

// Add Stylesheets
JHtml::_('stylesheet', 'template.css', array('version' => 'auto', 'relative' => true));

// Use of Google Font
if ($this->params->get('googleFont'))
{
	JHtml::_('stylesheet', 'https://fonts.googleapis.com/css?family=' . $this->params->get('googleFontName'));
	$this->addStyleDeclaration("
	h1, h2, h3, h4, h5, h6, .site-title {
		font-family: '" . str_replace('+', ' ', $this->params->get('googleFontName')) . "', sans-serif;
	}");
}

// Template color
if ($this->params->get('templateColor'))
{
	$this->addStyleDeclaration('
	body.site {
		border-top: 3px solid ' . $this->params->get('templateColor') . ';
		background-color: ' . $this->params->get('templateBackgroundColor') . ';
	}
	a {
		color: ' . $this->params->get('templateColor') . ';
	}
	.nav-list > .active > a,
	.nav-list > .active > a:hover,
	.dropdown-menu li > a:hover,
	.dropdown-menu .active > a,
	.dropdown-menu .active > a:hover,
	.nav-pills > .active > a,
	.nav-pills > .active > a:hover,
	.btn-primary {
		background: ' . $this->params->get('templateColor') . ';
	}');
}

// Check for a custom CSS file
JHtml::_('stylesheet', 'custom.css', array('relative' => true));

// Check for a custom js file
JHtml::_('script', 'user.js', array('version' => 'auto', 'relative' => true));

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Adjusting content width
$position7ModuleCount = $this->countModules('position-7');
$position8ModuleCount = $this->countModules('position-8');

if ($position7ModuleCount && $position8ModuleCount)
{
	$span = 'span6';
}
elseif ($position7ModuleCount && !$position8ModuleCount)
{
	$span = 'span9';
}
elseif (!$position7ModuleCount && $position8ModuleCount)
{
	$span = 'span9';
}
else
{
	$span = 'span12';
}

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
}
else
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />

	<link href="https://fonts.googleapis.com/css?family=Bree+Serif" rel="stylesheet">
</head>


<!-- <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> -->
<script>

$(document).ready(function(){
    $('.customer-logos').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
    });

	$('.readmore a').html('Read More');

	$('.blog .items-row .span6').removeClass('span6').addClass('span12');

	$('#mc_embed_signup_scroll label').html('Subscribe to our newsletter');
});
</script>

<body class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '')
	. ($this->direction === 'rtl' ? ' rtl' : '');
?>">
	<!-- Body -->
	<div class="body" id="top">
		<div class="<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">

		<?php if ($this->countModules('position-1')) : ?>
			
				<div class="headerBg">
					<div class="container">

						<div class="row">
					
							<div class="col-sm-3 col-xs-6 logoContainer">
								<a href="/">
									<img src="<?php echo $this->baseurl ?>/templates/protostar/images/carnival.svg" />
								</a>
							</div>

							<div class="col-sm-9">

								<div class="row">

									<div class="col-sm-12">

									<ul class="nav nav-pills topNav">
										<li role="presentation"><a href="/events" >Events
											<br /><span>Attractions</span>
										</a>
										
										</li>
										<li role="presentation"><a href="/events">Visitors
											<br /> <span>Tourist Guide</span>
										</a>
										
										</li>
										<li role="presentation"><a href="/volunteers">participate
											<br /> <span>Get Involved</span></a>
											
										</li>
										<li role="presentation"><a href="/sponsors">Sponsors
											<br /> <span>Supporters</span>
										</a>
												
										</li>
										<li role="presentation"><a href="/partners">Partners
											<br /> <span>Stakeholders</span>
											</a>
											
										</li>
										<li role="presentation"><a href="/vendors" >Vendors
										<br /><span>Marketplace</span>
										</a>
										
										</li>
										<li role="presentation"><a href="/events">Shop
											<br /> <span>Tickets</span>
										</a>
											
										</li>
									</ul>

									</div>

								</div>

								<div class="row">

									<div class="col-sm-12">

									<nav class="navigation" role="navigation">
										<div class="navbar pull-left">
											<a class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
												<span class="element-invisible"><?php echo JTEXT::_('TPL_PROTOSTAR_TOGGLE_MENU'); ?></span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
											</a>
										</div>
										<div class="nav-collapse">
											<jdoc:include type="modules" name="position-1" style="none" />
										</div>
									</nav>

									</div>

								</div>

							</div>

						

						</div>

					</div>

				</div>

		


			
				
			<?php endif; ?>
			
			<!-- Header -->
			<header class="header" role="banner">
				<div class="header-inner clearfix">
					<a class="brand pull-left" href="<?php echo $this->baseurl; ?>/">
						<?php echo $logo; ?>
						<?php if ($this->params->get('sitedescription')) : ?>
							<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription'), ENT_COMPAT, 'UTF-8') . '</div>'; ?>
						<?php endif; ?>
					</a>
					<div class="header-search pull-right">
						<jdoc:include type="modules" name="position-0" style="none" />
					</div>
				</div>
			</header>

			
			<jdoc:include type="modules" name="banner" style="xhtml" />
			
			<jdoc:include type="modules" name="fullwidth-1"   />
			<jdoc:include type="modules" name="fullwidth-2"   />
			<jdoc:include type="modules" name="fullwidth-3"   />
			<jdoc:include type="modules" name="fullwidth-4"   />
			<jdoc:include type="modules" name="fullwidth-5"   />
			<jdoc:include type="modules" name="fullwidth-6"   />
			<jdoc:include type="modules" name="fullwidth-7"  />
			<jdoc:include type="modules" name="fullwidth-8"  />

						
			<?php 
			$app = JFactory::getApplication();
			$menu = $app->getMenu();
			$menu_id = $menu->getActive()->id;

			?>

			<div class="row-fluid <?php if ($menu->getActive() != $menu->getDefault()) {
		
	echo "innerPage"; } ?>" >
				<?php if ($position8ModuleCount) : ?>
					<!-- Begin Sidebar -->
					<!-- <div id="sidebar" class="span3">
						<div class="sidebar-nav">
							<jdoc:include type="modules" name="position-8" style="xhtml" />
						</div>
					</div> -->
					<!-- End Sidebar -->
				<?php endif; ?>

				<div class="">
					<main id="content" role="main" class="container">
						
						<?php if ($position7ModuleCount) { ?>
						<div class="col-sm-9">
							<!-- Begin Content -->
								<jdoc:include type="modules" name="position-3" style="xhtml" />
							<jdoc:include type="message" />
							<jdoc:include type="component" />
							<div class="clearfix"></div>
							<jdoc:include type="modules" name="position-2" style="none" />
							<!-- End Content -->
						</div>

						<div id="aside" class="col-sm-3">
							<!-- Begin Right Sidebar -->
							<jdoc:include type="modules" name="position-7" style="well" />
							<!-- End Right Sidebar -->

							<div id="aside2" >
								<div class="well ">
									<h3 class="page-header">Public Relations</h3>
									<ul class="latestnews mod-list">
										<li itemscope="" itemtype="https://schema.org/Article">
											<a ">
												<span itemprop="name">
												Andrew Ricketts			</span>
											</a>
										</li>
										<li itemscope="" itemtype="https://schema.org/Article">
											<a href="mailto:andrew@totalpublicrelations.ca">
												<span itemprop="name">
												andrew@totalpublicrelations.ca			</span>
											</a>
										</li>
									
									</ul>
								</div>
															<!-- End Right Sidebar -->
								</div>
						</div>

						
						
						<?php } else { ?>

							<div class="col-sm-12">
							<!-- Begin Content -->
								<jdoc:include type="modules" name="position-3" style="xhtml" />
							<jdoc:include type="message" />
							<jdoc:include type="component" />
							<div class="clearfix"></div>
							<jdoc:include type="modules" name="position-2" style="none" />
							<!-- End Content -->
						</div>

						<?php } ?>

					
					</main>
				</div>
				
			</div>
		</div>
	</div>
	<!-- Footer -->
	
	<jdoc:include type="modules" name="footer_fullwidth"   />

	<?php
$app = JFactory::getApplication();
$menu = $app->getMenu();
if ($menu->getActive() == $menu->getDefault()) {
	?>
	<style>
		.row-fluid .container{
			display:none;
		}
		.osgalery-cat-tabs{
			display: none;
		}
		.n2-clear{
			display: none;
		}
	</style>
	<?php
}
?>
  


	<footer class="footer" role="contentinfo">
		<div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
			<hr />
			<jdoc:include type="modules" name="footer" style="none" />
			<p class="pull-right">
				<a href="#top" id="back-top">
					<?php echo JText::_('TPL_PROTOSTAR_BACKTOTOP'); ?>
				</a>
			</p>
			<p class="copyright">
				&copy; <?php echo date('Y'); ?> Toronto Caribbean Carnival. All Rights Reserved. | Website by <a href="nusoft.net">NUSOFT.NET INC</a>.
			</p>
		</div>
	</footer>
	<jdoc:include type="modules" name="debug" style="none" />

	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->

<script src="https://www.ticketgateway.com/js/sell_widget/listing-iframe-bootstrapper.js" ></script>
<script src="https://www.ticketgateway.com/js/sell_widget/api-embeded.js" ></script>
 
<script> 
  
var deadline = new Date("aug 03, 2019 15:37:25").getTime(); 
  
var x = setInterval(function() { 
  
var now = new Date().getTime(); 
var t = deadline - now; 
var days= Math.floor(t / (1000 * 60 * 60 * 24)); 
var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
var seconds = Math.floor((t % (1000 * 60)) / 1000); 
document.getElementById("day").innerHTML =days ; 
document.getElementById("hour").innerHTML =hours; 
document.getElementById("minute").innerHTML = minutes;  
document.getElementById("second").innerHTML =seconds;  

document.getElementById("day1").innerHTML =days ; 
document.getElementById("hour1").innerHTML =hours; 
document.getElementById("minute1").innerHTML = minutes;  
document.getElementById("second1").innerHTML =seconds;  


if (t < 0) { 
        clearInterval(x); 
        document.getElementById("demo").innerHTML = "TIME UP"; 
        document.getElementById("day").innerHTML ='0'; 
        document.getElementById("hour").innerHTML ='0'; 
        document.getElementById("minute").innerHTML ='0' ;  
        document.getElementById("second").innerHTML = '0'; } 
}, 1000); 

</script> 
</body>
</html>
