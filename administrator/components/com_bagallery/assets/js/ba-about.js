/**
* @package   BaGallery
* @author    Balbooa http://www.balbooa.com/
* @copyright Copyright @ Balbooa
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

jQuery(document).off('click.bs.tab.data-api click.bs.modal.data-api click.bs.collapse.data-api');

jQuery(document).ready(function(){

    function showNotice(message, className)
    {
        if (!className) {
            className = '';
        }
        if (notification.hasClass('notification-in')) {
            setTimeout(function(){
                notification.removeClass('notification-in').addClass('animation-out');
                setTimeout(function(){
                    addNoticeText(message, className);
                }, 400);
            }, 2000);
        } else {
            addNoticeText(message, className);
        }
    }

    function addNoticeText(message, className)
    {
        var time = 3000;
        if (className) {
            time = 6000;
        }
        notification.find('p').html(message);
        notification.addClass(className).removeClass('animation-out').addClass('notification-in');
        setTimeout(function(){
            notification.removeClass('notification-in').addClass('animation-out');
            setTimeout(function(){
                notification.removeClass(className);
            }, 400);
        }, time);
    }

    var update = jQuery('#update-data').val(),
        notification = jQuery('#ba-notification');

    setTimeout(function(){
        jQuery('.alert.alert-success').addClass('animation-out');
    }, 2000);

    update = JSON.parse(update);

    jQuery('#toolbar-cleanup-images').on('click', function(){
        jQuery('#cleanup-images-dialog').modal();
    });

    jQuery('#cleanup-images').on('click', function(){
        var str = update.saving+'<img src="'+update.url;
        str += 'administrator/components/com_bagallery/assets/images/reload.svg"></img>';
        notification.addClass('notification-in');
        notification.find('p').html(str);
        jQuery.ajax({
            type:"POST",
            dataType:'text',
            url:"index.php?option=com_bagallery&task=galleries.cleanup&tmpl=component",
            success: function(msg){
                showNotice(msg);
            }
        });
        jQuery('#cleanup-images-dialog').modal('hide');
    });

    jQuery('.ba-custom-select > i, div.ba-custom-select input').on('click', function(event){
        event.stopPropagation()
        var $this = jQuery(this),
            parent = $this.parent();
        jQuery('.visible-select').removeClass('visible-select');
        parent.find('ul').addClass('visible-select');
        parent.find('li').one('click', function(){
            var text = jQuery.trim(jQuery(this).text()),
                val = jQuery(this).attr('data-value');
            parent.find('input[type="text"]').val(text);
            parent.find('input[type="hidden"]').val(val).trigger('change');
        });
        parent.trigger('show');
        setTimeout(function(){
            jQuery('body').one('click', function(){
                jQuery('.visible-select').removeClass('visible-select');
            });
        }, 50);
    });

    jQuery('.ba-tooltip').each(function(){
        jQuery(this).parent().on('mouseenter', function(){
            var tooltip = jQuery(this).find('.ba-tooltip'),
                coord = this.getBoundingClientRect(),
                top = coord.top,
                data = tooltip.html(),
                center = (coord.right - coord.left) / 2;
                className = tooltip[0].className;
            center = coord.left + center;
            if (tooltip.hasClass('ba-bottom')) {
                top = coord.bottom;
            }
            jQuery('body').append('<span class="'+className+'">'+data+'</span>');
            var tooltip = jQuery('body > .ba-tooltip').last(),
                width = tooltip.outerWidth(),
                height = tooltip.outerHeight();
            if (tooltip.hasClass('ba-top') || tooltip.hasClass('ba-help')) {
                top -= (15 + height);
                center -= (width / 2)
            }
            if (tooltip.hasClass('ba-bottom')) {
                top += 10;
                center -= (width / 2)
            }
            tooltip.css({
                'top' : top+'px',
                'left' : center+'px'
            }).on('mousedown', function(event){
                event.stopPropagation();
            });
        }).on('mouseleave', function(){
            var tooltip = jQuery('body').find(' > .ba-tooltip');
            tooltip.addClass('tooltip-hidden');
            setTimeout(function(){
                tooltip.remove();
            }, 500);
        });
    });

    jQuery('div.ba-custom-select').on('show', function(){
        jQuery(this).find('i.zmdi.zmdi-check').parent().addClass('selected');
    })

    jQuery('#toolbar-language button').on('click', function(){
        if (!window.languageList) {
            jQuery.ajax({
                type:"POST",
                dataType:'text',
                url:"index.php?option=com_bagallery&task=galleries.getLanguagesList",
                success: function(msg){
                    window.languageList = JSON.parse(msg);
                    var str = '';
                    languageList.forEach(function(el, ind){
                        str += '<div class="language-line"><span class="language-img"><img src="';
                        str += el.flag+'"></span><span class="language-title" data-key="';
                        str += ind+'">'+el.title+'</span><span class="language-code">'+el.code+'</span></div>';
                    });
                    jQuery('#language-dialog .languages-wrapper').append(str);
                    jQuery('#language-dialog').modal();
                }
            });
        } else {
            jQuery('#language-dialog').modal();
        }
    });

    jQuery('.languages-wrapper').on('click', '.language-title', function(){
        var url = languageList[this.dataset.key].url,
            str = update.installing+'<img src="'+update.url;
        str += 'administrator/components/com_bagallery/assets/images/reload.svg"></img>';
        notification.addClass('notification-in');
        notification.find('p').html(str);
        jQuery('#language-dialog').modal('hide');
        jQuery.ajax({
            type:"POST",
            dataType:'text',
            url:"index.php?option=com_bagallery&task=galleries.addLanguage&tmpl=component",
            data:{
                ba_url: url,
            },
            success: function(msg){
                showNotice(msg)
            }
        });
    });

    jQuery('#toolbar-about').find('button').on('click', function(){
        jQuery('#about-dialog').modal();
    });
    
    var massage = '';
    
    jQuery('.update-link').on('click', function(event){
        event.preventDefault();
        updateComponent();
    });
    
    jQuery('.leave-feedback').on('click', function(event){
        event.stopPropagation();
        event.preventDefault();
        jQuery('#feedback-dialog').modal();
    });
    
    jQuery('#feedback-dialog').on('show', function(){
        jQuery('#about-dialog').modal('hide');
        jQuery('.feedback-body').show();
        jQuery('.happy-feedback, .not-happy-feedback').hide();
    });
    
    jQuery('.happy-rewiev').on('click', function(event){
        event.stopPropagation();
        event.preventDefault();
        jQuery('.happy-feedback').show();
        jQuery('.feedback-body, .not-happy-feedback').hide();
    });
    
    jQuery('.not-happy-rewiev').on('click', function(event){
        event.stopPropagation();
        event.preventDefault();
        jQuery('.not-happy-feedback').show();
        jQuery('.feedback-body, happy-feedback').hide();
    });

    var iframe,
        uploadMode;

    jQuery('#update-dialog').on('show', function(){
        window.addEventListener("message", listenMessage, false);
        uploadMode = 'update';
    });

    jQuery('#update-dialog').on('hide', function(){
        window.removeEventListener("message", listenMessage, false);
        iframe.remove();
    });

    function updateComponent()
    {
        var src = 'https://www.balbooa.com/demo/index.php?option=com_baupdater&view=bagallery';
        iframe = jQuery('<iframe/>', {
                name : 'update-target',
                id : 'update-target',
                src : src
        });
        iframe.appendTo(jQuery('#form-update'));
        iframe.css('width', '440px')
        iframe.css('height', '290px')
        jQuery('#update-dialog').modal();
    }

    function listenMessage(event)
    {
        if (event.origin == 'https://www.balbooa.com' || event.origin == location.origin) {
            if (uploadMode == 'update') {
                var link = event.data;
                iframe.remove();
                jQuery('#update-dialog').modal('hide');
                var flag = link[0] + link[1] + link[2] + link[3];
                if (flag == 'http') {
                    jQuery('.ba-update-message').addClass('animation-out');
                    setTimeout(function(){
                        var str = update.const+'<img src="'+update.url;
                        str += 'administrator/components/com_bagallery/assets/images/reload.svg"></img>';
                        notification.addClass('notification-in');
                        notification.find('p').html(str);
                    }, 400);
                    jQuery.ajax({
                        type:"POST",
                        dataType:'text',
                        url:"index.php?option=com_bagallery&task=galleries.updateGallery&tmpl=component",
                        data:{
                            target:link,
                        },
                        success: function(msg){
                            var result = JSON.parse(msg);
                            jQuery('.update').text(result.version);
                            showNotice(result.message)
                        }
                    });
                } else {
                    showNotice(link);
                }
            }
        }
    }
});

